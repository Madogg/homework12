
;@homework 12
;@R0, R1, R2, R3 - used for function arguments and holding values. 
 
;@R4 - row
	row_i		.req R4
;@R5 - col_i
	col_i		.req R5
;@R6 - running sums
	sums			.req R6


		.global main
main:
	LDR R0, =name
	BL printf			@Display your first and last name
	
_show_time:				@show current date and time using date command
	LDR R0, =date
	BL system

@seed rand with day of year
_seed_rand:
@ Get the number of seconds since the Epoch
	MOV R0, #0			@ 1st argument to time is NULL or zero
	BL time				@ Get the number of seconds since the Epoch
	LDR R1, =epoch		@ Save in memory
	STR R0, [R1]
	
@ Get the day of the year and add one
	LDR R0, =epoch
	BL localtime
	LDR R1, =broken_down	@ Save address of broken down time
	STR R0, [R1]
	LDR R1, [R0, #28]	@ Get the day of the year from broken down time
	ADD R1, R1, #1		@ You need to add 1 because Jan 1 is 0
	MOV R0, R1
	BL srand
@create 2x4 array and populate with numbers 0-127
_populate:

@ for (row_i=0, row_i<2, row_i++) {
	MOV row_i, #0		@initialize row index
_rows:					
@ for (col_i=0, col_i<4, col_i++) {
	MOV col_i, #0		@initialize column index
_columns:			
	
	BL rand				@generate random number
	
	MOV R1, #rand_max
	BL divide			@divide and get remainder to get number between 0 and 127
	
	MOV R2, R1 
	MOV R0, row_i
	MOV R1, col_i
	BL set				@store random number in array at (row_i, col_i)
	ADD col_i, col_i, #1
	CMP col_i, #4
	BLT _columns
@	}
	ADD row_i, row_i, #1
	CMP row_i, #2
	BLT _rows
@	}
@dump values using dump enty in array.s
array_dump:
	BL dump				@call the dump funtion from array.s
@sum values in (0,0), (0,2), (1,1), (1,3)
Fetch_sum:
	MOV sums, #0		@initilize sums = 0
	;@fetch and add (0,0) to sums
	MOV R0, #0
	MOV R1, #0
	BL get
	ADD sums, sums, R0
	;@fetch and add (0,2) to sums
	MOV R0, #0
	MOV R1, #2
	BL get
	ADD sums, sums, R0
	;@fetch and add (1,1) to sums
	MOV R0, #1
	MOV R1, #1
	BL get
	ADD sums, sums, R0
	;@fetch and add (1,3) to sums
	MOV R0, #1
	MOV R1, #3
	BL get
	ADD sums, sums, R0
	
	LDR R0, =sum_result
	MOV R1, sums
	BL printf			@Print result of summing the data points
	
_exit:					@clean exit
	MOV R7, #1
	SWI 0
	
.data
name:
	.asciz "Mark Dyer\n"
sum_result:
	.asciz "The sum of (0,0), (0,2), (1,1) and (1,3) is: %d\n"
epoch:
	.word 0
broken_down:
	.word 0
date:
	.asciz "date"

	.equ rand_max, 128
